# (Random Password Generator) - v1.0.3
# 2022.07.09. - #7779d1

import random
import datetime
import sys
from time import process_time
from rich.console import Console
from rich.table import Table

print(""".................^5G7.............................
................:PB&B:........... ................
................Y#G5JP~:JYYYYJ~^..:........  .....
................Y&&J?#7^&###&BBGG!BG57^:... ......
........... ..:?5Y5~!55~JPGGPPGY^.75GGP5J~  ... ..
........^~^^~?#&5YYYGB#BGGGPPPGP55Y5P55PGBY^......
.......^!~~!YP&BP5Y5&##BBBBBGGBBBGPGPPPPGGGGJ.....
.::^^~~Y?7!JBGBP5Y?G&&##&&##BBBBBBGBBGGGGGGG&B^...
^7P?!~7PJ7?5BB55Y??&@&BGGB###&#&BB#&#&BGG#BB#@&~..
~?Y!~!YGY7JPPP555JG&5!~~!7P&&&&&##&&&&&#B#&##&@&J.
~77!!75555PPP5Y5PPB577?5B&#&@&#&&&P#GJ&@##@@&&@YJY
!??!!?55555YYJ?JJ7?YY5&B5&&&@PJ&@&?J?~5&&#@@@&&#.:
JJ5JJYYJYJJ???77!7?Y5Y#B7G@@P7Y#BPY?7!75&&@&@&P&~.
Y5PY7?5JYYYYJ????JY5Y7?P!JP5?J5PB&&&BY7!&@&@@#P#^.
JJYYJYYY55&&BGPPGGGPPP5J777777?YJB##P!!5@&&&57G&:.
JJJJJYY7J?G@@@&&BPPG@&#5?77!!~~~~7JJ?7!B&&#JY#P^..
JJJYY?^..~:JB@@&GY77GBG7!!!!~~~~!!~~~~~G#B##&?....
JYY?^.......~#@@&5??7!!!!7??~~~~~~~~~~~P&&B!^.....
??^..........7#&@#?7!!!!77?J?7~~~~~~~!7B&&5.......
^............~~7P##5?777777??J?!~~~!!JG5#&5.......
.............:..:?#5&B5J??77?JJ7!!!JPPP.Y@P.......
.................:~:PB&BGP5YJ77!7JPPJJ7.?@G.......
......................YGPGGGGPPPP5J7~JJ.?@G.......
................::~!?5P5YYY5PPP5?!!~~7J?G@G.......""")

cmdTable = Table(title='Navi(Random Password Generator) - v1.0.3')

cmdTable.add_column('Commands', no_wrap = True)

cmdTable.add_row('Gen. Pswd.-- (/gen)')
cmdTable.add_row('Save file -- (/save)')
cmdTable.add_row('Print Pswd. -- (/print)')
cmdTable.add_row('Quit -- (/quit)')
print('---------------------------------------------------------')

console = Console()
console.print(cmdTable)

today = datetime.datetime.now()
todayStr = str(today)

run = True

while run: 
    print('---------------------------------------------------------')
    cmdIn = input('>>> ')
    if cmdIn == '/gen':
        lenOfPass = int(input('Lenght of password: '))
        
        rndList = []
        genPswd = ''

        process_start = process_time()

        for char in range (0, lenOfPass):
            randChar = random.randint(33, 126)
            rndList.append(randChar)

        for i in range(0, lenOfPass):
            intToSym = chr(rndList[i])
            genPswd = genPswd + intToSym            
        
        process_stop = process_time()

        print('---------------------------------------------------------')
        print('New password: ', genPswd)
        print('---------------------------------------------------------')
        print('Elapsed time: ', 'Start:', process_start, 'End: ', process_stop)        
        print('Elapsed time during the whole program in seconds: ', process_stop-process_start)        
        print('Date: ', today)
            
    elif cmdIn == '/save':
        saveDir = input('Chooes directory: ')
        saveFile = input('Save as: ')
        print(saveDir + saveFile + todayStr)
        genFile = open(saveDir + saveFile + '.txt', 'w')
        genFile.write('\n' + 'Navi(Random Password Generator) - v1.0.3' +
                      '\n' + todayStr +
                      '\n' + 'Path: ' + saveDir +
                      '\n' + 'File name: ' + saveFile +
                      '\n' + 'Random Password: ' +
                      '\n' + genPswd)
        genFile.close()
        print('---------------------------------------------------------')
        print('File saved')
        print('---------------------------------------------------------')
    elif cmdIn == '/print':

        print(genPswd)
    elif cmdIn == '/quit':
        sys.exit()




