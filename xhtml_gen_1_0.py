
#xhtml 1.1 skeleton generator 
#2022 -- #7779d1

from rich.console import Console
from rich.table import Table
import sys
import datetime

print('-------------------------')
cmdTable = Table(title='XHTML Skeleton Generator v1.0.0')

cmdTable.add_column('Commands', no_wrap = True)

cmdTable.add_row('Save file -- (/save)')
cmdTable.add_row('Quit -- (/quit)')

console = Console()
console.print(cmdTable)

run = True
today = datetime.datetime.now()

while run:
    print('-----------------------')
    cmdInput = input('Enter command: ' )
    if cmdInput == '/quit':
        sys.exit()
    elif cmdInput == '/save':
        path = input('Directory ')
        fileName = input('Save as: ')
        print('--------------------------------------')
        print('File saved: ' + path + fileName, today )
        file = open(path + fileName + '.html', 'w')
        file.write('\n' + '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">'+
                   '\n' + '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">'
                   '\n' + '<head>' +
                   '\n' + '    <meta http-equiv="content-type" content="text/html: charset=UTF-8" />' +
                   '\n' + '    <link rel="stylesheet" type="text/css" href="./css/style.css" />' +
                   '\n' + '    <link rel="icon" href="./img/test.gif" />' +
                   '\n' + '    <title>Document</title>' +
                   '\n' + '</head>' +
                   '\n' + '<body>' +
                   '\n' + '</body>' +
                   '\n' + '</html>')
        file.close()
